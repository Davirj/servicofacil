<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Chamado;

class PrestadorViewController extends Controller
{
    public function index(){
        return view('prestador.home');
    }

    public function servicos(){
        $user = \Auth::user();
        $servicos = $user->servicosPrestados;

        return view('prestador.servicos', ['chamados' => $servicos]);
    }

    public function updateServico($id){
        $chamado = Chamado::find($id);

        if($chamado->status == 1){
            $chamado->status = 2;
        }elseif($chamado->status == 3){
            $chamado->status = 2;
        }elseif($chamado->status == 2){
            $chamado->status = 4;
        }

        $chamado->save();

        return redirect()->route('servico.prestador');
    }

    public function updateServicoPendente($id){
        $chamado = Chamado::find($id);

        $chamado->status = 3;
        $chamado->save();

        return redirect()->route('servico.prestador');
    }
}
