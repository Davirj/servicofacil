<?php

namespace App\Http\Controllers\Auth;

use Session;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ActivationController extends Controller
{
    public function activate(Request $request){
        
       $user = User::where('email', $request->email)->where('token', $request->token)->firstOrFail();
        
        $user->update([
            'active' => true,
            'token' => null
        ]);

        Session::flash('success', 'A sua conta foi ativada com sucesso! Por favor, realize o login.');        
        
        return redirect()->route('login');        
    }
}
