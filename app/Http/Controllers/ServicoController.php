<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Chamado;
use App\Models\Servico;
use App\Models\Tipo_Servico;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class ServicoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('servico.servico_home', ['chamados' => Auth::user()->chamados()->paginate(5), 'saldo' => Auth::user()->saldo]);
    }

    public function solicitar(){
        return view('servico.servico_form', [
            'servicos' => Servico::all(), 
            'tipos' => Tipo_Servico::all(),
            'saldo' => Auth::user()->saldo
        ]);
    }

    public function solicita(Request $request){
        $validados = $request->validate([
            'servico' => 'required',
            'tipo_servico' => 'required',
        ]);
        
        $user = Auth::user();

        //
        $prestadores = User::where('prestador', true)->get();        
        //
        
        $qtd_serv = count(User::find(5)->servicosPrestados);
        $prestador_id = 5;

        foreach ($prestadores as $prestador) {
            $cont = count($prestador->servicosPrestados);
            if($cont < $qtd_serv){
                $prestador_id = $prestador->id;
            }
        }        

        $chamado = new Chamado();
        
        $chamado->user_id = $user->id;
        $chamado->servico_id = $request->servico;
        $chamado->tipo_servico_id = $request->tipo_servico;
        $chamado->prestador_id = $prestador_id;
        

        $chamado->save();

        $preco = Tipo_Servico::find($request->tipo_servico)->preco;
        $saldo = Auth::user()->saldo;
        $cobrado = $preco - $saldo;
        $restante;

        if($cobrado < 0){
            $cobrado = $cobrado*-1;
            $restante = $saldo - $cobrado;
        }else{
            $restante = 0;
        }
        
        $user->saldo = $restante;
        
        $user->save();

        return redirect()->route('servico');
    }
}
