<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Servico extends Model
{
    protected $fillable = [
        'nome'
    ];

    public function tipo_servicos(){
        return $this->hasMany('App\Models\Tipo_Servico');
    }
}
