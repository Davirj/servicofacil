<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tipo_Servico extends Model
{
    protected $fillable = [
        'nome', 'servico_id'
    ];

    public function servico(){
        return $this->belongsTo('App\Models\Servico');        
    }
}