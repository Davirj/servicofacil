<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chamado extends Model
{
    protected $fillable = [
        'user_id', 'servico_id', 'tipo_servico_id', 'prestador_id'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function prestador(){
        return $this->belongsTo('App\User', 'user_id', 'prestador_id');
    }

    public function servico(){
        return $this->belongsTo('App\Models\Servico');
    }

    public function tipo_servico(){
        return $this->belongsTo('App\Models\Tipo_Servico');
    }
}
