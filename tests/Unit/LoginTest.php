<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{
    /**
     * Teste de acesso ao sistema.
     *
     * @return void
     */
    public function testLogin()
    {   
        //Testando login
        $user = User::find(1);
        $response = $this->actingAs($user)->get('/home');
        $response->assertStatus(200);
    }
}
