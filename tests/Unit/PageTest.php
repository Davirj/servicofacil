<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PageTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        //Testa se está redirecinado para a pagina de login para
        //usuários não logados
        $response = $this->get('/');
        $response->assertRedirect('/login');

        //Tenta acessar a página home sem estar logado
        $response = $this->call('GET', '/home');
        $response->assertStatus(302);
        $response->assertRedirect('/login');

        //Acessa a página login sem estar logado
        $response = $this->call('GET', '/login');        
        $response->assertStatus(200);
        $response->assertSeeTextInOrder(['Login', 'E-Mail', 'Senha']);
        

        //Logado raiz do site

        $user = User::find(1);      

        $response = $this->actingAs($user)->get('/');
        $response->assertStatus(200);
        $response->assertSeeText('Home page');
        $response->assertSeeText('Serviços');
        $response->assertSeeText('Dashboard');

        //Logado página serviço
        $response = $this->actingAs($user)->get('/servico');
        $response->assertStatus(200);
        $response->assertSeeText('Home page');
        $response->assertSeeText('Serviços');
        $response->assertSeeText('Serviços solicitados');
        $response->assertSeeTextInOrder(['Código', 'Servico', 'Tipo do servico']);

        //Logado solicitar serviço
        $response = $this->actingAs($user)->get('/servico');
        $response->assertStatus(200);
        $response->assertSeeText('Home page');
        $response->assertSeeText('Serviços');
        $response->assertSeeText('Serviço');
        $response->assertSeeText('Tipo do servico');
        $response->assertSeeText('Solicitar');

    }    
}
