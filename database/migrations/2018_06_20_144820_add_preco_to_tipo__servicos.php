<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPrecoToTipoServicos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tipo__servicos', function (Blueprint $table){
            $table->float('preco', 6 , 3)->default(100,00);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tipo__servicos', function(Blueprint $table){
            $table->dropColumn(['preco']);
        });
    }
}
