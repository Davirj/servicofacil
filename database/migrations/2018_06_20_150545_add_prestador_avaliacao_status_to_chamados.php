<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPrestadorAvaliacaoStatusToChamados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('chamados', function (Blueprint $table) {
            $table->integer('prestador_id')->unsigned();
            
            $table->foreign('prestador_id')->references('id')->on('users');
            
            $table->integer('avaliacao')->default(0);
            $table->integer('status')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chamados', function(Blueprint $table){
            $table->dropColumn(['prestador_id', 'avaliacao', 'status']);
        });
    }
}
