<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChamadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chamados', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('servico_id')->unsigned();
            $table->integer('tipo_servico_id')->unsigned();


            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('servico_id')->references('id')->on('servicos');
            $table->foreign('tipo_servico_id')->references('id')->on('tipo_servicos');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chamados');
    }
}
