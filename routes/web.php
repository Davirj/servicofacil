<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/auth/activate', 'Auth\ActivationController@activate')->name('auth.activate');

Auth::routes();
Route::get('/register/prestador', 'Auth\PrestadorController@register')->name('registra.prestador');

Route::get('/servico/change/{id}', 'PrestadorViewController@updateServico');
Route::get('/servico/change/pendente/{id}', 'PrestadorViewController@updateServicoPendente');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/servico_prestador', 'PrestadorViewController@servicos')->name('servico.prestador');
Route::get('/servico', 'ServicoController@index')->name('servico');
Route::get('/servico/solicitar', 'ServicoController@solicitar')->name('solicitar');
Route::post('/servico/solicita', 'ServicoController@solicita')->name('solicita');