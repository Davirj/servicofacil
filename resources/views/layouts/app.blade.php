<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Serviço Fácil</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <script
			  src="https://code.jquery.com/jquery-3.3.1.js"
			  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
			  crossorigin="anonymous"></script>
</head>
<body>
    <div id="app">
        
        @include('partials.navbar')

        <main class="py-4">
            

            @if(Session::has('success') || Session::has('error'))
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-8">
                            @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                            @else
                            <div class="alert alert-danger">
                                {{ Session::get('error') }}
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            @endif
            
            <div class="container">
                <div class="row">
                    @if(Auth::check())
                    <div class="col-md-2">
                        @include('partials.aside')
                    </div>
                    @endif
                    
                    @if(Auth::check())
                    <div class="col-md-10">
                    @else
                    <div class="col-md-10 offset-1">
                    @endif
                        @yield('content')            
                    </div>
                </div>
            </div>

            
        </main>

        @yield('script')

    </div>
</body>
</html>
