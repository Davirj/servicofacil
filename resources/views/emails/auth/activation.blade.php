@component('mail::message')
# Ative a sua conta!

Nós recebemos o seu cadastro, e agora precisamos confirmar!

@component('mail::button', ['url' => route('auth.activate', [
        'token' => $user->token,
        'email' => $user->email
])])
    Ativar
@endcomponent

Obrigado,<br>
    Serviço Fácil
@endcomponent
