@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header"><b>Solicitar Serviço</b></div>

    <div class="card-body">
    <form method="POST" action="{{ route('solicita') }}">
            @csrf

            <div class="form-group row">
                <label for="servico" class="col-md-4 col-form-label text-md-right">Serviço</label>

                <div class="col-md-6">
                    
                    <select class="custom-select" id="servico" name="servico" required>
                        <option value="null">Selecione...</option>`
                    </select>

                    @if ($errors->has('servico'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('servico') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="tipo_servico" class="col-md-4 col-form-label text-md-right">Tipo do Serviço</label>

                <div class="col-md-6">
                    
                    <select class="custom-select" id="tipo_servico" name="tipo_servico" required>
                        
                    </select>

                    @if ($errors->has('tipo_servico'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('tipo_servico') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            
            <div class="form-group row">
                <div class="col-md-12 text-center" >
                    <h3>Crédito: {{ $saldo }}</h3>
                    <span id="info_serv"></span><br>
                    <span id="info_total"></span>
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        Confirmar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    

</script>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            var servicos =<?php echo json_encode($servicos) ?>;
            var tipo_serv = <?php echo json_encode($tipos) ?>;
            var credito = <?php echo $saldo ?>;
            var info_serv = $('#info_serv').html('');
            var info_total = $('#info_total').html('');

            $('#tipo_servico').html("");

            servicos.forEach(function(valorServico,indexServico,arrayServico){
                $("#servico").html($("#servico").html()+`<option value="${valorServico.id}">${valorServico.nome}</option>`);
                
                tipo_serv[indexServico].tipo = [];
                
                tipo_serv.forEach(function(valorTipo,indexTipo,arrayTipo){
                    if(valorServico.id==valorTipo.servico_id){
                        tipo_serv[indexServico].tipo.push(valorTipo);

                    }
                });
            })

            $("#servico").change(function(){
                $("#tipo_servico").html("");
                
                tipo_serv.forEach(element => {
                    if(element.servico_id == $(this).find(':selected').val()){
                        $("#tipo_servico").html( $("#tipo_servico").html()+ `<option value="${element.id}">${element.nome}</option>` )
                        
                        info_serv.html('Preço servico: ' + element.preco);
                        var cobrado = element.preco - credito;
                        if(cobrado < 0) cobrado = 0;
                        info_total.html('Valor cobrado: ' + cobrado.toFixed(2) )
                    }
                })
            });

            $("#tipo_servico").change(function(){
                
                var selecionado = $(this).find(':selected');
                
                tipo_serv.forEach(ele => {
                    if(ele.id == selecionado.val()){
                        
                        info_serv.html('Preço servico: ' + ele.preco);
                        var cobrado = ele.preco - credito;
                        if(cobrado < 0) cobrado = 0;
                        info_total.html('Valor cobrado: ' + cobrado.toFixed(2) )                     
                        
                    }
                })
                
            });           
        });
    </script>
@endsection