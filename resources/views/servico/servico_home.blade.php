@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header"><b>Serviços solicitados</b></div>

    <div class="card-body">
        <div class="col-md-3 alert-info text-center" style="border-radius: 10px">
            <span><h3>Crédito: {{ $saldo }}</h3></span>
        </div>

        <table class="table table-hover ">
            <thead>
                <tr>
                    <td>Número</td>
                    <td>Servico</td>
                    <td>Tipo do servico</td>
                    <td>Data</td>
                    <td>Status</td>
                </tr>
            </thead>

            <tbody>
                @foreach($chamados as $chamado)
                    <tr>
                        <td>{{$chamado->id}}</td>
                        <td>{{$chamado->servico->nome}}</td>
                        <td>{{$chamado->tipo_servico->nome}}</td>
                        <td>{{ $chamado->created_at}}</td>
                        <td>
                            @if($chamado->status == 1)
                                Aguardando início
                            @elseif($chamado->status == 2)
                                Em execução
                            @elseif($chamado->status == 3)
                                Pendente
                            @else
                                Finalizado
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div>
            {{ $chamados->links() }}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-12">
        <a href="{{ route('solicitar') }}" class="btn btn-success">Solicitar Serviço</a>
    </div>    
</div>

@endsection