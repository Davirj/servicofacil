@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header"><b>Serviços solicitados</b></div>

    <div class="card-body">
        <table class="table table-hover ">
            <thead>
                <tr>
                    <td>Número</td>
                    <td>Servico</td>
                    <td>Tipo do servico</td>
                    <td>Data</td>
                    <td>Status</td>
                    <td>Ação</td>
                </tr>
            </thead>

            <tbody>
                @foreach($chamados as $chamado)
                    @if($chamado->status != 4)
                    <tr>
                        <td>{{$chamado->id}}</td>
                        <td>{{$chamado->servico->nome}}</td>
                        <td>{{$chamado->tipo_servico->nome}}</td>
                        <td>{{ $chamado->created_at}}</td>
                        <td>
                            @if($chamado->status == 1)
                                Aguardando início
                            @elseif($chamado->status == 2)
                                Em execução
                            @elseif($chamado->status == 3)
                                Pendente
                            @else
                                Finalizado
                            @endif
                        </td>
                        <td>
                            @if($chamado->status == 1)
                                <a href="servico/change/{{$chamado->id}}">Iniciar</a>
                            @elseif($chamado->status == 2)
                                <a href="servico/change/{{$chamado->id}}">Finalizar</a> <br>
                                <a href="servico/change/pendente/{{$chamado->id}}">Deixar pendente</a>
                            @elseif($chamado->status == 3)
                                <a href="servico/change/{{$chamado->id}}">Continuar</a>
                            @endif
                        </td>
                    </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection