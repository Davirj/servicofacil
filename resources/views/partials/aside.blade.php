<a href="{{route('home')}}" class="btn btn-block btn-primary">Home page</a>

@if(Auth::user()->prestador == 1)
<a href="{{route('servico.prestador')}}" class="btn btn-block btn-primary">Serviços</a>
@else
<a href="{{route('servico')}}" class="btn btn-block btn-primary">Serviços</a>
@endif